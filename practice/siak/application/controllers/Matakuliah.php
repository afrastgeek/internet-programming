<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matakuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('GeneralModel');
        $this->load->helper('url');
        $this->load->helper('form');
    }

    public function index($offset = 0)
    {
        $this->load->library('pagination');
        $matkul = $this->db->get('matakuliah');
        $row = $matkul->num_rows();
        $config['base_url'] = site_url('matakuliah/index/');
        $config['total_rows'] = $row;
        $config['per_page'] = 5;


//config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'Pertama';
        $config['last_link'] = 'Akhir';
        $config['prev_tag_open'] = '<li class="prev" aria-label="Previous">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $matkul = $this->db->get('matakuliah', 5, $offset);
        $data['matkul'] = $matkul->result();
        $data['no'] = $offset+1;
        $this->load->view('matakuliah', $data, false);
    }

    public function addToCart()
    {
        if($this->session->has_userdata('cart')) {
            $data = $this->session->cart;

            $data[] = array(
                'kode'      => $this->input->post('kode_matakuliah'),
                'nama'      => $this->input->post('nama_matakuliah'),
                'sks'      => $this->input->post('sks')
                );

            $this->session->set_userdata('cart', $data);
        } else {
            $data = array(
                cart => array(
                    'kode'      => $this->input->post('kode_matakuliah'),
                    'nama'      => $this->input->post('nama_matakuliah'),
                    'sks'      => $this->input->post('sks')
                )
            );

            $this->session->set_userdata($data);
        }

        redirect('matakuliah');
    }

    public function viewCart()
    {
        $this->load->view('cart');
    }

    public function get($id)
    {
        $query = $this->db->get_where(‘matakuliah’, array(‘’=>$id));
        return $query->row();
    }
}

/* End of file matakuliah.php */
/* Location: ./application/controllers/matakuliah.php */
