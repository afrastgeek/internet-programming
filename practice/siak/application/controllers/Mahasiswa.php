<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('GeneralModel');
	}

	public function index($offset = 0)
	{
		$this->load->library('pagination');
		$mhs = $this->db->get('mahasiswa');
		$row = $mhs->num_rows();
		$config['base_url'] = site_url('mahasiswa/index/');
		$config['total_rows'] = $row;
		$config['per_page'] = 5;


		//config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'Petama';
        $config['last_link'] = 'Akhir';
        $config['prev_tag_open'] = '<li class="prev" aria-label="Previous">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$mhs = $this->db->get('mahasiswa', 5, $offset);
		$data['mhs'] = $mhs->result();
		$data['no'] = $offset+1;
		$this->load->view('mahasiswa', $data, FALSE);
	}

	function tambahMhs()
	{
		$this->load->view('tambahmhs', $data);
	}

}

/* End of file mahasiswa.php */
/* Location: ./application/controllers/mahasiswa.php */