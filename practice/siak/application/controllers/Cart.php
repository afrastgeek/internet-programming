<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

 class Cart extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Matakuliah');
        $this->load->library('cart');
        $this->load->library('template');
    }

    function add($kode_matakuliah) {
        $matakuliah = $this->matakuliah->get($kode_matakuliah);

        $data = array(
            'id'      => $matakuliah->id,
            'kode_matakuliah'     => $matakuliah->kode_matakuliah,
            'nama_matakuliah'    => $matakuliah->nama_matakuliah,
            'sks'   => $matakuliah->sks,

        );

        $this->cart->insert($data);
        // redirect('cart');
    }

    function update()
    {
        $this->cart->update($_POST);
        // redirect('cart');
    }

    function index() {
        $data['cart_list'] = $this->cart->contents();
        $this->template->display('cart', $data);
    }

 }
 ?>
