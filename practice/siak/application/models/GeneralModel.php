<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralModel extends CI_Model {

	function getAll($table)
	{
		$r = $this->db->get($table);
		return $r->result();
	}

	function getRow($table, $where)
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$r = $this->db->get($table);
		return $r->row();
	}

	function insert($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}

	function update($table, $where, $data)
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	function delete($table, $where)
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($table);
		return $this->db->affected_rows();
	}

}

/* End of file generalModel.php */
/* Location: ./application/models/generalModel.php */