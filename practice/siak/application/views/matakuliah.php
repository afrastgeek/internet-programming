<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
<head>
	<title>MataKuliah</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
	<script src="<?php echo base_url('assets/js/jquery-3.2.0.min.js') ?>"></script>
</head>
<body>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">SIAK</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class=""><a href="<?php echo site_url('mahasiswa'); ?>">Mahasiswa <span class="sr-only">(current)</span></a></li>
	        <li><a href="<?php echo site_url('matakuliah'); ?>">Mata Kuliah</a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pilihan Mata Kuliah <span class="caret"></span></a>
	          <?php if(isset($_SESSION['cart'])) : ?>
	          <ul class="dropdown-menu">
	          	<?php foreach ($_SESSION['cart'] as $value): ?>
	          		<li><?php echo $value['kode'] . " - " . $value['nama'] . " - " . $value['sks']; ?></li>
	          	<?php endforeach ?>
	          </ul>
	        	<?php endif; ?>
	        </li>
	      </ul>
	      <form class="navbar-form navbar-left">
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Search">
	        </div>
	        <button type="submit" class="btn btn-default">Submit</button>
	      </form>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="#">Link</a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Action</a></li>
	            <li><a href="#">Another action</a></li>
	            <li><a href="#">Something else here</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	          </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="page-header">
			<h2>Data Mahasiswa</h2>
		</div>

		<?php
			if ($matkul != NULL) {
		?>
				<table class="table table-striped table-hover">
					<tr>
						<th>No</th>
						<th>Kode Mata Kuliah</th>
						<th>Nama Mata Kuliah</th>
						<th>sks</th>
						<th>Aksi</th>
					</tr>
		<?php
				foreach ($matkul as $value) {
		?>
				<form action="<?php echo base_url('index.php/matakuliah/addToCart'); ?>" method="post">
					<input type="hidden" name="kode_matakuliah" value="<?php echo $value->kode_matakuliah; ?>">
					<input type="hidden" name="nama_matakuliah" value="<?php echo $value->nama_matakuliah; ?>">
					<input type="hidden" name="sks" value="<?php echo $value->sks; ?>">
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $value->kode_matakuliah; ?></td>
						<td><?php echo $value->nama_matakuliah; ?></td>
						<td><?php echo $value->sks; ?></td>
						<td><a href="#">Edit</a> | <a href="#">Hapus</a> |
						<input type="submit" name="submit" value="Tambah ke Cart">
					</tr>
				</form>
		<?php
					$no++;
				}
		?>
				</table>
				<div class="pull-right">
					<?php echo $pagination; ?>
				</div>
		<?php
			}
		?>
	</div>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>
</html>
