<?php
  /*
  file    : mhs.php
  tentang : deklarasi class+atribut+fungsi
  */

  // membuat definisi class
  class mhs {
    var $nim;
    var $nama;
    var $kelas;
    var $angkatan;

    /*
     * Try Out 2.1: Fatal error: Uncaught Error: Cannot access private property
     * mhs::$nim . Stack trace: #0 {main} thrown.
     */
    // private $nim;
    // private $nama;
    // private $kelas;
    // private $angkatan;

    /*
     * Try Out 2.2: No Error.
     */
    // public $nim;
    // public $nama;
    // public $kelas;
    // public $angkatan;

    /*
     * Try Out 2.3: Fatal error: Uncaught Error: Cannot access protected
     * property mhs::$nim . Stack trace: #0 {main} thrown.
     */
    // protected $nim;
    // protected $nama;
    // protected $kelas;
    // protected $angkatan;

    function printing(){

    /*
     * Try Out 2.4: Fatal error: Uncaught Error: Call to private method
     * mhs::printing() from context '' . Stack trace: #0 {main} thrown.
     */
    // private function printing(){

    /*
     * Try Out 2.5: Fatal error: Uncaught Error: Call to protected method
     * mhs::printing() from context '' in . Stack trace: #0 {main} thrown.
     */
    // protected function printing(){

    /*
     * Try Out 2.6: No Error.
     */
    // public function printing(){
      echo "NIM       : $this->nim <br/>";
      echo "Nama      : $this->nama <br/>";
      echo "Kelas     : $this->kelas <br/>";
      echo "Angkatan  : $this->angkatan <br/>";
    }
  }

  //membuat objek
  $m1 = new mhs();

  //mengakses atribut
  $m1->nim="150111";
  $m1->nama="Ali";
  $m1->kelas="C1";
  $m1->angkatan="2015";

  //mengakses fungsi
  $m1->printing();
