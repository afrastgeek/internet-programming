<?php
  /*
  file    : Pelanggan.php
  tentang : deklarasi class+atribut+fungsi
  */

  // membuat definisi class
  class Pelanggan {
    public $kode_pelanggan;
    public $nama;
    public $kode_transaksi;

    public function printing(){
      echo "Kode Pelanggan  : $this->kode_pelanggan <br/>";
      echo "Nama Pelanggan  : $this->nama <br/>";
      echo "Kode Transaksi  : $this->kode_transaksi <br/>";
    }
  }

  //membuat objek
  $p1 = new Pelanggan();

  //mengakses atribut
  $p1->kode_pelanggan=random_int(0,1477);
  $p1->nama="Ali";
  $p1->kode_transaksi=random_int(0,1477);

  //mengakses fungsi
  $p1->printing();
