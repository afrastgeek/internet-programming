<?php
  /*
  file    : buku.php
  tentang : deklarasi class
  */

  //membuat definisi class
  class buku {
    /*
     * Deprecated: Methods with the same name as their class will not be
     * constructors in a future version of PHP;
     */
    // function buku(){

    /* Parse error: syntax error, unexpected 'buku' (T_STRING), expecting '(' */
    // function __construct buku(){

    function __construct(){
        echo "Ini buku <br />";
    }

  }

  //memakai class
  $b1 = new buku();
  $book = new buku();
  $latihan = new buku();
  $catatan = new buku();
