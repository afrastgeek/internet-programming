<?php
include_once "mhs_model.php";

class mhs_controller{
    var $model;
	
	function mhs_controller(){
		$this->model = new mhs_model();
		$this->terjemahkanAksi();
	}
	
	function terjemahkanAksi(){
		if (isset($_GET['kode'])) {
		    $kode =  $_GET['kode'];
			switch ($kode) {
			  case 'add' : $this->addNewRecord();
			               break; 
			  case 'add_nove' : 
			               $this->saveNewRecord();
			               break; 
			  case 'print' : 
			               header("location:print.php");
			               break; 
			  default   : $this->displayAllRecord();
			               break; 
			}
		
		} else {
		    $this->displayAllRecord();
		}
	}
    
	function addNewRecord(){
		header("location:mhs_add_new_record.php");
	}
	
	function displayAllRecord(){
	    $data = $this->model->getAllRecord();
		include_once "mhs_view.php";
	}
	
	
	function saveNewRecord(){
		if (isset($_POST['save'])) {
		    $nim= $_POST['nim'];
		    $nama= $_POST['nama'];
		    $kelas= $_POST['kelas'];
			
			$this->model->insert($nim,$nama,$kelas);
		}
		$this->displayAllRecord();
	
	}

}
?>